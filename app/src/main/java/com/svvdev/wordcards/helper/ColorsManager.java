package com.svvdev.wordcards.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;

import com.svvdev.wordcards.BuildConfig;


public class ColorsManager {

    /**
     * chooses a random color from material_colors.xml
     */
    public static int getRandomMaterialColor(String typeColor, Context context) {
        int returnColor = Color.GRAY;
        int arrayId = context.getResources().getIdentifier("mdcolor_" + typeColor, "array", BuildConfig.APPLICATION_ID);

        if (arrayId != 0) {
            TypedArray colors = context.getResources().obtainTypedArray(arrayId);
            int index = (int) (Math.random() * colors.length());
            returnColor = colors.getColor(index, Color.GRAY);
            colors.recycle();
        }
        return returnColor;
    }

}
