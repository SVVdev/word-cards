package com.svvdev.wordcards.screens.wordrepeat.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.svvdev.wordcards.R;

public class WordCardFragment extends Fragment {
    // Store instance variables
    private String word;
    private String translation;
    private TextView cardText;
    private CardView cardView;
    private boolean isFaceSide = true;

    // newInstance method for creating fragment
    public static WordCardFragment newInstance(String word, String translation) {

        Bundle args = new Bundle();

        args.putString("word", word);
        args.putString("translation", translation);

        
        WordCardFragment fragment = new WordCardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        word = getArguments().getString("word");
        translation = getArguments().getString("translation");
    }

    @Nullable
    @Override
    // Inflate the view for the fragment based on layout XML
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle params) {
        View view = inflater.inflate(R.layout.item_viewpager_wordcard, container, false);
        cardText = (TextView) view.findViewById(R.id.cardText);
        updateCardText(word);

        cardView = (CardView) view.findViewById(R.id.cardView);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFaceSide){
                    cardText.setText(translation);
                    isFaceSide = false;
                } else {
                    cardText.setText(word);
                    isFaceSide = true;
                }
            }
        });

        return view;
    }

    public void updateCardText(String cardText) {
        this.cardText.setText(cardText);
        this.isFaceSide = true;
    }

    public void setCurrentWord(String word, String translation){
        this.word = word;
        this.translation = translation;
        this.isFaceSide = true;

        cardText.setText(word);
    }

}
