package com.svvdev.wordcards.screens.wordadd;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.svvdev.wordcards.data.AppDatabase;
import com.svvdev.wordcards.data.words.Word;

/**
 * ViewModels are entities that are free of the Activity/Fragment lifecycle.
 * For example, they can save their state/data even during an screen orientation change.
 *
 * part of Architecture Components
 */

public class AddWordViewModel extends AndroidViewModel {

    private AppDatabase appDatabase;

    public AddWordViewModel(@NonNull Application application) {
        super(application);

        this.appDatabase = AppDatabase.getAppDatabase(this.getApplication());
    }

    public void addWord(Word word){
        new addAsyncTask(appDatabase).execute(word);
    }

    private class addAsyncTask extends AsyncTask<Word, Void, Void>{
        private AppDatabase db;

        public addAsyncTask(AppDatabase appDatabase) {
            this.db = appDatabase;
        }

        @Override
        protected Void doInBackground(Word... word) {
            db.wordDao().insertAll(word);
            return null;
        }
    }
}
