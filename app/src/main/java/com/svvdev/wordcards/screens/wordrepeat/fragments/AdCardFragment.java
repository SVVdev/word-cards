package com.svvdev.wordcards.screens.wordrepeat.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.svvdev.wordcards.R;

public class AdCardFragment extends Fragment {
    // Store instance variables
    AdView adView;

    // newInstance method for creating fragment
    public static AdCardFragment newInstance() {

        Bundle args = new Bundle();

        AdCardFragment fragment = new AdCardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    // Inflate the view for the fragment based on layout XML
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_viewpager_adcard, container, false);

        adView = view.findViewById(R.id.adView);

        return view;
    }

    public void placeFreshAd(){
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

}
