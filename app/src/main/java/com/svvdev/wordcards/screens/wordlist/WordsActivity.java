package com.svvdev.wordcards.screens.wordlist;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.svvdev.wordcards.R;
import com.svvdev.wordcards.data.AppDatabase;
import com.svvdev.wordcards.data.words.Word;
import com.svvdev.wordcards.screens.wordadd.AddWordActivity;
import com.svvdev.wordcards.screens.wordupdate.UpdateWordActivity;

import java.util.ArrayList;
import java.util.List;

/**
 *  Manages screen with RecyclerView
 *
 */
public class WordsActivity extends AppCompatActivity implements WordsAdapter.MessageAdapterListener {
    private WordsViewModel wordsViewModel;
    private AppDatabase appDatabase;
    private List<Word> words = new ArrayList<Word>();
    private RecyclerView recyclerView;
    private WordsAdapter wordsAdapter;
    private ActionModeCallback actionModeCallback;
    private ActionMode actionMode;


    // public static RecyclerView wordsRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Hide the navigation bar.
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);

        // floating btn
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WordsActivity.this, AddWordActivity.class));
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        wordsAdapter = new WordsAdapter(this, words, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        // TODO remove deviders!
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(wordsAdapter);


        wordsViewModel = ViewModelProviders.of(this).get(WordsViewModel.class);
        wordsViewModel.getWordsList().observe(
                WordsActivity.this,
                new Observer<List<Word>>() {
            @Override
            public void onChanged(@Nullable List<Word> wordsUpdated) {
                    // TODO Possible optimization via refactoring and inserting "words" to the "WordsAdapter"
                    words.clear();
                    words.addAll(wordsUpdated);
                    wordsAdapter.notifyDataSetChanged();
            }
        });

        actionModeCallback = new ActionModeCallback();


    }



    @Override
    public void onIconClicked(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }

        toggleSelection(position);
    }

    @Override
    public void onIconImportantClicked(int position) {
        // Star icon is clicked,
        // mark the message as important
        Word word = words.get(position);
        word.setImportant(!word.isImportant());

        // adding changes to the DB
        // RecyclerView notified via LiveData
        wordsViewModel.updateWord(word);

    }

    @Override
    public void onMessageRowClicked(int position) {
        // verify whether action mode is enabled or not
        // if enabled, change the row state to activated
        if (wordsAdapter.getSelectedItemCount() > 0) {
            enableActionMode(position);
        } else {
            // start activity to update word data
            String wordId = String.valueOf(words.get(position).getWid());

            Intent intent = new Intent(this, UpdateWordActivity.class);
            intent.putExtra("WORD_ID", wordId);
            startActivity(intent);
        }
    }

    @Override
    public void onRowLongClicked(int position) {
        // long press is performed, enable action mode
        enableActionMode(position);
    }


    private class ActionModeCallback implements android.support.v7.view.ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(android.support.v7.view.ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.main_screen_menu_action_mode, menu);

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                //set gray color to status bar
//                getWindow().setStatusBarColor(getResources().getColor(R.color.bg_action_mode));
//            }
            return true;
        }

        @Override
        public boolean onPrepareActionMode(android.support.v7.view.ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(android.support.v7.view.ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:
                    // delete all the selected messages
                    deleteMessages();
                    mode.finish();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(android.support.v7.view.ActionMode mode) {
            wordsAdapter.clearSelections();

            //swipeRefreshLayout.setEnabled(true);
            actionMode = null;
            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    wordsAdapter.resetAnimationIndex();
                    // mAdapter.notifyDataSetChanged();
                }
            });

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                //return to non action color of status bar
//                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
//            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_screen_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            Toast.makeText(getApplicationContext(), "Search...", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        wordsAdapter.toggleSelection(position);
        int count = wordsAdapter.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }


    // deleting the messages from recycler view
    private void deleteMessages() {
        wordsAdapter.resetAnimationIndex();
        List<Word> selectedWords =
                wordsAdapter.getSelectedWords();
        wordsViewModel.deleteWords(selectedWords);
    }


    public List<Word> getWords() {
        return words;
    }

    public WordsAdapter getWordsAdapter() {
        return wordsAdapter;
    }
}
