package com.svvdev.wordcards.screens.wordrepeat;

import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.svvdev.wordcards.R;
import com.svvdev.wordcards.data.AppDatabase;
import com.svvdev.wordcards.data.words.Word;
import com.svvdev.wordcards.helper.NonSwipeableViewPager;
import com.svvdev.wordcards.helper.SmartFragmentStatePagerAdapter;
import com.svvdev.wordcards.screens.wordrepeat.fragments.AdCardFragment;
import com.svvdev.wordcards.screens.wordrepeat.fragments.WordCardFragment;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class RepeatWordActivity extends AppCompatActivity {
    private SmartFragmentStatePagerAdapter adapterViewPager;
    private NonSwipeableViewPager viewPager;
    private Button btnNext;

    private List<Word> wordsToRepeat;
    private ListIterator<Word> iterator;
    private Word currentWord;

    private int counter = 0;
    private static final int SHOW_AD_ON = 5;
    private static final int DELAY_ON_AD = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repeat_word);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // get all words to repeat
        try{
            wordsToRepeat = new getWordsToRepeatTask(this).execute().get();
        }catch (Exception e){
            // TODO display toast as another thread
            Toast.makeText(this, R.string.error_cant_get_words_to_repeat,Toast.LENGTH_SHORT).show();
            finish();
        }
        Collections.shuffle(wordsToRepeat);
        iterator = wordsToRepeat.listIterator();


        viewPager = (NonSwipeableViewPager) findViewById(R.id.viewPager);
        adapterViewPager = new RepeatWordAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapterViewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0){
                    // for the word card
                    updateWordCard();
                } else {
                    // for the ad card
                    updateAdCard();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counter++;
                int index = viewPager.getCurrentItem();
                if ((index == 0) && (counter % SHOW_AD_ON == 0)){
                    // for the ad card
                    viewPager.setCurrentItem(1);
                    btnNext.setText(R.string.skip_ad);
                    doDelay(DELAY_ON_AD);
                }
                else {
                    // for the word card
                    viewPager.setCurrentItem(0);
                    if (index == 0){ // to fix issue when moving 0 --> 0 onPageSelected is not called
                        updateWordCard();
                    }
                    btnNext.setText(R.string.next_card);
                }
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void doDelay(int seconds) {
        btnNext.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.disabled_btn)));
        btnNext.setTextColor(getResources().getColor(R.color.disabled_btn_text));
        btnNext.setEnabled(false);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                btnNext.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
                btnNext.setTextColor(getResources().getColor(R.color.white));
                btnNext.setEnabled(true);
            }
        }, seconds * 1000);   //
    }

    private void updateWordCard() {
        if (!iterator.hasNext()){
            Collections.shuffle(wordsToRepeat);
            iterator = wordsToRepeat.listIterator();
        }
            currentWord = iterator.next();
            WordCardFragment currentFragment = (WordCardFragment)adapterViewPager.getRegisteredFragment(0);
            currentFragment.setCurrentWord(currentWord.getWordName(), currentWord.getTranslation());
    }
    private void updateAdCard(){
        // load ad
        AdCardFragment currentFragment = (AdCardFragment)adapterViewPager.getRegisteredFragment(1);
        currentFragment.placeFreshAd();
    }

    // access db in another thread
    private static class getWordsToRepeatTask extends AsyncTask<Void,Void,List<Word>> {
        private WeakReference<RepeatWordActivity> activityReference;

        public getWordsToRepeatTask(RepeatWordActivity context){
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected List<Word> doInBackground(Void... params) {
            // get a reference to the activity if it is still there
            RepeatWordActivity activity = activityReference.get();

            if (activity == null) return null;

            return AppDatabase.getAppDatabase(activity).wordDao().getAllAsList();
        }
    }

    // Extend from SmartFragmentStatePagerAdapter for more dynamic ViewPager items
    public static class RepeatWordAdapter extends SmartFragmentStatePagerAdapter {
        private static final int NUM_ITEMS = 2;


        public RepeatWordAdapter(FragmentManager fragmentManager){
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }


        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: // Fragment # 0 - This will show WordCardFragment
                        return WordCardFragment.newInstance(
                                "Let's start!",
                                "Начнем!");
                case 1: // Fragment # 0 - This will show AdCardFragment
                    return AdCardFragment.newInstance();
                default:
                    return null;
            }
        }

    }

}
