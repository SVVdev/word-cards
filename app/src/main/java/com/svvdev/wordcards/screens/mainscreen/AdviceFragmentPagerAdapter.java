package com.svvdev.wordcards.screens.mainscreen;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.svvdev.wordcards.data.AppDatabase;
import com.svvdev.wordcards.data.advices.Advice;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class AdviceFragmentPagerAdapter extends FragmentStatePagerAdapter {
    List<AdviceFragment> fragments;
    Context              context;

    public AdviceFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        this.fragments = new ArrayList<>();

        Observable.just(AppDatabase.getAppDatabase(context))
            .subscribeOn(Schedulers.io())
            .subscribe(appDatabase -> {
                List<Advice> advices = appDatabase.adviceDao().getAll();
                for (Advice advice : advices) {
                    AdviceFragment adviceFragment = new AdviceFragment();
                    adviceFragment.setTitle(advice.getTitle());
                    adviceFragment.setBody(advice.getBody());
                    fragments.add(adviceFragment);
                }
                notifyDataSetChanged();
            });
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object fragment = super.instantiateItem(container, position);
        fragments.set(position, (AdviceFragment) fragment);
        return fragment;
    }

}
