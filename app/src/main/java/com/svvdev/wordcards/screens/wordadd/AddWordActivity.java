package com.svvdev.wordcards.screens.wordadd;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.svvdev.wordcards.R;
import com.svvdev.wordcards.data.words.Word;
import com.svvdev.wordcards.helper.ColorsManager;

import java.util.Calendar;

public class AddWordActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private EditText wordEditText;
    private EditText translationEditText;

    private AddWordViewModel addWordViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_word);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Hide the navigation bar.
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        wordEditText = (EditText) findViewById(R.id.wordEditText);
        translationEditText = (EditText) findViewById(R.id.translationEditText);

        addWordViewModel = ViewModelProviders.of(this).get(AddWordViewModel.class);

        // focus on word name and open keyboard
        wordEditText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

    }

    // Menu icons are inflated just as they were with actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_word_screen_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.saveWord:
                if (fieldsAreFilled()){
                    addWordViewModel.addWord(new Word(
                            formatText(wordEditText.getText().toString()),
                            formatText(translationEditText.getText().toString()),
                            ColorsManager.getRandomMaterialColor("500",this),
                            false,
                            Calendar.getInstance().getTime()
                    ));
                    finish();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     *
     * @param s
     * @return first char capitalized string (other part is in lower case)
     */
    private String formatText(String s) {
        return Character.toUpperCase(s.charAt(0)) + s.substring(1).toLowerCase();
    }

    private boolean fieldsAreFilled() {
        if (TextUtils.isEmpty(wordEditText.getText().toString().trim())){
            Toast.makeText(this, R.string.insert_word, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(translationEditText.getText().toString().trim())){
            Toast.makeText(this, R.string.insert_translation, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

}
