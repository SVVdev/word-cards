package com.svvdev.wordcards.screens.wordupdate;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.svvdev.wordcards.data.AppDatabase;
import com.svvdev.wordcards.data.words.Word;

/**
 * ViewModels are entities that are free of the Activity/Fragment lifecycle.
 * For example, they can save their state/data even during an screen orientation change.
 *
 * part of Architecture Components
 */

public class UpdateWordViewModel extends AndroidViewModel {

    private AppDatabase appDatabase;

    public UpdateWordViewModel(@NonNull Application application) {
        super(application);

        this.appDatabase = AppDatabase.getAppDatabase(this.getApplication());
    }

    public void updateWord(Word word){
        new updateAsyncTask(appDatabase).execute(word);
    }

    private class updateAsyncTask extends AsyncTask<Word, Void, Void>{
        private AppDatabase db;

        public updateAsyncTask(AppDatabase appDatabase) {
            this.db = appDatabase;
        }

        @Override
        protected Void doInBackground(Word... word) {
            db.wordDao().updateWord(word[0]);
            return null;
        }
    }
}
