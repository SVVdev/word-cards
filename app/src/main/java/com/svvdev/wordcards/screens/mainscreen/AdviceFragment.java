package com.svvdev.wordcards.screens.mainscreen;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.svvdev.wordcards.R;


public class AdviceFragment extends Fragment {

    private String title;
    private String body;

    public static Fragment getInstance() {
        return new AdviceFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.item_advices, container, false);

        TextView title = rootView.findViewById(R.id.advice_text_title);
        TextView body = rootView.findViewById(R.id.advice_text_body);

        title.setText(getTitle());
        body.setText(getBody());

        return rootView;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
