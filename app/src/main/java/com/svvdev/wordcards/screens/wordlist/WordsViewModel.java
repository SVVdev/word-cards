package com.svvdev.wordcards.screens.wordlist;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.svvdev.wordcards.data.AppDatabase;
import com.svvdev.wordcards.data.words.Word;

import java.util.List;

/**
 * ViewModels are entities that are free of the Activity/Fragment lifecycle.
 * For example, they can save their state/data even during an screen orientation change.
 *
 * part of Architecture Components
 */

public class WordsViewModel extends AndroidViewModel {

    private final LiveData<List<Word>> wordsList;

    private AppDatabase appDatabase;

    public WordsViewModel(Application application){
        super(application);
        // we can only get getApplication() context with AndroidViewModel
        appDatabase = AppDatabase.getAppDatabase(this.getApplication());

        wordsList = appDatabase.wordDao().getAll();
    }

    public LiveData<List<Word>> getWordsList(){
        return wordsList;
    }

    public void deleteWords(List<Word> words){
        new deleteAsyncTask(appDatabase).execute(words);
    }

    public void updateWord(Word word){
        new updateAsyncTask(appDatabase).execute(word);
    }

    private class updateAsyncTask extends AsyncTask<Word, Void, Void>{
        private AppDatabase db;

        public updateAsyncTask(AppDatabase appDatabase) {
            this.db = appDatabase;
        }

        @Override
        protected Void doInBackground(Word... word) {
            db.wordDao().insertAll(word);
            return null;
        }
    }

    private class deleteAsyncTask extends AsyncTask<List<Word>, Void, Void> {
        private AppDatabase db;

        public deleteAsyncTask(AppDatabase appDatabase) {
            this.db = appDatabase;
        }

        @Override
        protected Void doInBackground(List<Word>[] wordsLists) {
            db.wordDao().delete(wordsLists[0]);
            return null;
        }
    }
}
