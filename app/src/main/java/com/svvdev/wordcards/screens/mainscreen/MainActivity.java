package com.svvdev.wordcards.screens.mainscreen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.android.gms.ads.MobileAds;
import com.svvdev.wordcards.R;
import com.svvdev.wordcards.data.helper.Initializer;
import com.svvdev.wordcards.screens.wordlist.WordsActivity;
import com.svvdev.wordcards.screens.wordrepeat.RepeatWordActivity;

import me.relex.circleindicator.CircleIndicator;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize data
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (!prefs.getBoolean("firstTimeStart", false)) {
            // init db with default values
            new Initializer(getApplicationContext()).execute();

            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("firstTimeStart", true);
            editor.apply();
        }

        // AdMob
        MobileAds.initialize(this, "ca-app-pub-9799381290345976~8633552231");

        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        setSupportActionBar(toolbar);

        // Hide the navigation bar.
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);

        // implementation of viewpager with advices
        ViewPager viewPager = findViewById(R.id.adviceViewPager);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.steps);
        AdviceFragmentPagerAdapter pagerAdapter = new AdviceFragmentPagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(pagerAdapter);
        try{
            Thread.sleep(2000);
        }catch (Exception e){
            System.out.println("hello");
        }
        indicator.setViewPager(viewPager);

        View myCardsBtn = findViewById(R.id.my_cards_btn);
        View repeatBtn = findViewById(R.id.repeat_btn);

        myCardsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, WordsActivity.class));
            }
        });

        repeatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, RepeatWordActivity.class));
            }
        });

    }


}
