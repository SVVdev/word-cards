package com.svvdev.wordcards.screens.wordupdate;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.svvdev.wordcards.R;
import com.svvdev.wordcards.data.AppDatabase;
import com.svvdev.wordcards.data.words.Word;

import java.lang.ref.WeakReference;

public class UpdateWordActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private Word wordToUpdate;
    private EditText wordEditText;
    private EditText translationEditText;

    private UpdateWordViewModel updateWordViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_word);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Hide the navigation bar.
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String wordId = getIntent().getStringExtra("WORD_ID");
        try {
            wordToUpdate = new getWordTask(this).execute(wordId).get();
        }catch (Exception e){
            // TODO display toast as another thread
            Toast.makeText(this, R.string.error_cant_get_word,Toast.LENGTH_SHORT).show();
            finish();
        }

        wordEditText = (EditText) findViewById(R.id.wordEditText);
        translationEditText = (EditText) findViewById(R.id.translationEditText);
        // fill data
        wordEditText.setText(wordToUpdate.getWordName());
        translationEditText.setText(wordToUpdate.getTranslation());

        updateWordViewModel = ViewModelProviders.of(this).get(UpdateWordViewModel.class);

        // focus on translation and open keyboard
        translationEditText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    // access db in another thread
    private static class getWordTask extends AsyncTask<String,Void,Word>{
        private WeakReference<UpdateWordActivity> activityReference;

        public getWordTask(UpdateWordActivity context){
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected Word doInBackground(String... strings) {
            // get a reference to the activity if it is still there
            UpdateWordActivity activity = activityReference.get();

            if (activity == null) return null;

            return AppDatabase.getAppDatabase(activity).wordDao().getById(strings[0]);
        }
    }

    // Menu icons are inflated just as they were with actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_word_screen_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.saveWord:
                if (fieldsAreFilled() && dataChanged()){
                    wordToUpdate.setWordName(formatText(wordEditText.getText().toString()));
                    wordToUpdate.setTranslation(formatText(translationEditText.getText().toString()));
                    updateWordViewModel.updateWord(wordToUpdate);
                    finish();
                } else {
                    finish();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean dataChanged() {
        if (wordToUpdate.getWordName().toLowerCase().equals(wordEditText.getText().toString().trim().toLowerCase())
                &&
                wordToUpdate.getTranslation().toLowerCase().equals(translationEditText.getText().toString().trim().toLowerCase())){
            return false;
        }else {
            return true;
        }
    }

    /**
     *
     * @param s
     * @return first char capitalized string (other part is in lower case)
     */
    private String formatText(String s) {
        s = s.trim();
        return Character.toUpperCase(s.charAt(0)) + s.substring(1).toLowerCase();
    }

    private boolean fieldsAreFilled() {
        if (TextUtils.isEmpty(wordEditText.getText().toString().trim())){
            Toast.makeText(this, R.string.insert_word, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(translationEditText.getText().toString().trim())){
            Toast.makeText(this, R.string.insert_translation, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

}
