package com.svvdev.wordcards.data.helper;

import android.content.Context;
import android.os.AsyncTask;

import com.svvdev.wordcards.R;
import com.svvdev.wordcards.data.AppDatabase;
import com.svvdev.wordcards.data.advices.Advice;
import com.svvdev.wordcards.data.words.Word;
import com.svvdev.wordcards.helper.ColorsManager;

import java.util.Calendar;
import java.util.Date;

public class Initializer extends AsyncTask<Void, Void, Void> {
    private Context context;

    public  Initializer(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        Date currentTime = Calendar.getInstance().getTime();

        String[] words = context.getResources().getStringArray(R.array.words_words);
        String[] translations = context.getResources().getStringArray(R.array.words_translations);
        for (int i = 0; i < words.length; i++) {
            Word word = new Word();
            word.setWordName(words[i]);
            word.setTranslation(translations[i]);
            word.setColor(ColorsManager.getRandomMaterialColor("500", context));
            word.setCreationDate(currentTime);
            appDatabase.wordDao().insertWord(word);
        }

        String[] titles = context.getResources().getStringArray(R.array.advice_titles);
        String[] bodies = context.getResources().getStringArray(R.array.advice_bodies);
        for (int i = 0; i < titles.length; i++) {
            appDatabase.adviceDao().insert(new Advice(titles[i], bodies[i]));
        }
        return null;
    }
}
