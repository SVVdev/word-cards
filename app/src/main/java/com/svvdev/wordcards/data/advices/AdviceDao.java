package com.svvdev.wordcards.data.advices;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface AdviceDao {

    @Query("SELECT * FROM advices") List<Advice> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE) void insert(Advice advice);

}
