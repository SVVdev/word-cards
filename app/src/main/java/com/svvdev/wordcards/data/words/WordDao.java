package com.svvdev.wordcards.data.words;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;


@Dao
public interface WordDao {

    @Query("SELECT * FROM words")
    LiveData<List<Word>> getAll();

    @Query("SELECT * FROM words")
    List<Word> getAllAsList();

    @Query("SELECT * FROM words WHERE wid LIKE :wid")
    Word getById(String wid);

    @Query("SELECT * FROM words WHERE wid IN (:userIds)")
    List<Word> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM words WHERE word_name LIKE :wordName LIMIT 1")
    Word findByName(String wordName);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Word... words);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Word> list);

    @Insert
    void insertWord(Word word);

    @Update
    void updateWord(Word word);

    @Delete
    void delete(List<Word> words);
}
