package com.svvdev.wordcards.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.svvdev.wordcards.data.advices.Advice;
import com.svvdev.wordcards.data.advices.AdviceDao;
import com.svvdev.wordcards.data.words.Word;
import com.svvdev.wordcards.data.words.WordDao;


@Database(entities = {Word.class, Advice.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public abstract WordDao wordDao();

    public abstract AdviceDao adviceDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "words-database").build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
