package com.svvdev.wordcards.data.words;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.svvdev.wordcards.data.helper.DateConverter;

import java.util.Date;

@Entity(tableName = "words")
public class Word {

    @PrimaryKey(autoGenerate = true)
    private int wid;

    @ColumnInfo(name = "word_name")
    private String wordName;

    @ColumnInfo(name = "translation")
    private String translation;

    @ColumnInfo(name = "color")
    private int color = -1;

    @ColumnInfo(name = "is_important")
    private boolean isImportant;

    @ColumnInfo(name = "creation_date")
    @TypeConverters(DateConverter.class)
    private Date creationDate;



    public Word(){

    }

    public Word(String wordName, String translation){
        setWordName(wordName);
        setTranslation(translation);
    }

    public Word(String wordName, String translation, int color, boolean isImportant, Date creationDate){
        setWordName(wordName);
        setTranslation(translation);
        setColor(color);
        setImportant(isImportant);
        setCreationDate(creationDate);
    }

    public int getWid() {
        return wid;
    }

    public void setWid(int wid) {
        this.wid = wid;
    }

    public String getWordName() {
        return wordName;
    }

    public void setWordName(String wordName) {
        this.wordName = wordName;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isImportant() {
        return isImportant;
    }

    public void setImportant(boolean important) {
        isImportant = important;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
